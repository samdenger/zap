<?php
class appoint {
	protected $config;
	public function __construct($config) {
		$this->config = $config;
	}
	
	public function GetPatientByIIN($IIN) {
		$post_params1 = array();
		$ch1 = curl_init($this->config['host']);
		curl_setopt($ch1, CURLOPT_POST, true);
		curl_setopt( $ch1, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch1, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch1, CURLOPT_POSTFIELDS, $post_params1);
		curl_setopt($ch1, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
		curl_setopt($ch1, CURLOPT_USERPWD, $this->config['login'] . ":" . $this->config['pass']);
		curl_setopt($ch1, CURLOPT_URL, $this->config['host']."clinic/hs/Services/GetPatientByIIN?IIN=$IIN");       
		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
			
		$response = curl_exec( $ch1 );
		curl_close( $ch1 );
 
		$response=json_decode($response);
	return $response;
		
	}
	public function GetDoctorID($IIN){
		$json = $this->GetPatientByIIN($IIN);
		$DoctorID = $json->DoctorID;
		return $DoctorID;
	}
	public function GetShedule($DoctorID){
		$post_params1 = array();
		$ch1 = curl_init($this->config['host']);
		curl_setopt($ch1, CURLOPT_POST, true);
		curl_setopt( $ch1, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch1, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch1, CURLOPT_POSTFIELDS, $post_params1);
		curl_setopt($ch1, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
		curl_setopt($ch1, CURLOPT_USERPWD, $this->config['login'] . ":" . $this->config['pass']);
		curl_setopt($ch1, CURLOPT_URL, $this->config['host']."clinic/hs/Services/GetShedule?DoctorID=$DoctorID");       
		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
			
		$response = curl_exec( $ch1 );
		curl_close( $ch1 );
 
		$response=json_decode($response);
	return $response;
		
	}
	public function SaveAppointment($post_params){//если передовать массив значений то функция не работает
			$post_params1 = array();
			$add = $this->config['host']."clinic/hs/Services/SaveAppointment";
			$ch1 = curl_init($add);
			curl_setopt($ch1, CURLOPT_POST, 1);
			curl_setopt( $ch1, CURLOPT_SSL_VERIFYHOST, false );
			curl_setopt( $ch1, CURLOPT_SSL_VERIFYPEER, false );
			//curl_setopt($ch1, CURLOPT_POSTFIELDS, $post_params1);
			curl_setopt($ch1, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
			curl_setopt($ch1, CURLOPT_USERPWD, $this->config['login'] . ":" . $this->config['pass']);
			curl_setopt($ch1, CURLOPT_POSTFIELDS, $post_params);
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true );
			$response = curl_exec( $ch1 );
			curl_close( $ch1 );
			$response=json_decode($response);
			return $response;
	}
	public function SaveAppointment1($iin,$DoctorID,$CabinetID,$StartTime,$EndTime,$Date,$language){
		$post_params1 = array();
		$ch1 = curl_init($this->config['host']);
		curl_setopt($ch1, CURLOPT_POST, true);
		curl_setopt( $ch1, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch1, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch1, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
		curl_setopt($ch1, CURLOPT_USERPWD, $this->config['login'] . ":" . $this->config['pass']);
		curl_setopt($ch1, CURLOPT_URL, $this->config['host']."clinic/hs/Services/SaveAppointment?IIN=$iin&DoctorID=$DoctorID&CabinetID=$CabinetID&Date=$Date&TimeStart=$StartTime&TimeEnd=$EndTime&RecordingMethod=2&Language=$language");
		curl_setopt($ch1, CURLOPT_POSTFIELDS, $post_params1);		
		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec( $ch1 );
		curl_close( $ch1 );
		$response=json_decode($response);
	return $response;	
	}
	public function GetError ($CodeError){
		if($CodeError!=="0"){
			
			return $error;
		}
		else {
			return true;
		}
	}

}