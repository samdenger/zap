<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">

	<title>Запись на приём online</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="path/to/image.jpg">
	<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
	<link rel="stylesheet" media="print" type="text/css" href="css/print.css" />
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#000">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#000">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#000">

	<style>html { background-color: #fff; } .g-recaptcha div { width:auto; height:auto; }</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body> 
	<div id="body">
	<div class="main">
	
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-md-offset-7">
					<div class="lang">
						<span id="langselect">Выбор языка:</span>
						<a class="lang-switch" id="kz">KZ</a>
						<a class="lang-switch active" id="ru">RU</a>
					</div>
				</div>
			</div>


			<div class="row text-center">
				<h3 id="clinic">КГП на ПХВ "Городская поликлиника №3" КГУ "УЗ акимата СКО</h3>
				<h1 id="recordOnline">Запись на приём online</h1>

				<div class="col-md-8 col-md-offset-2">
					<i class="fa fa-spinner fa-spin fa-3x fa-fw" id="loading" style="display:none;"></i>
					<span class="iin" id="iinform">
					<div class="form-group">
						<label id="iin_label">ИИН:</label>
						<input autofocus type="text" id="iin" placeholder="Введите ИИН"></div>
						<button class="btn" id="confirm">Далее <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
					</span>
				</div>
				<div class="col-md-6 col-md-offset-3 descript ">
					<div class="alert" id="error" style="display:none;">
						<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
						<span id="ernote">Текст ошибки</span>
					</div> 
					<p id="info">* Возможна только первичная запись на прием </p>
				</div>
				<div class="col-md-6 col-md-offset-3 " id="mainform" style="display:none;">
					<div class="talon">

						<div class="talon-body">
							<h3 id="LabelRecord">Запись на прием к врачу</h3>
							<table>
								<tr id="fio-label">
									<td class="text-right" id="FIOLabel">ФИО:</td>
									<td></td>
									<td id="fio"></td>
								</tr>
								<tr id="territory-label">
									<td class="text-right" id="LabelTerritory">Приписан:</td>
									<td></td>
									<td id="territory">Терапевтический Участок 4</td>
								</tr>
								<tr id="doctor-label">
									<td class="text-right" id="DoctorLabel">Врач:</td>
									<td></td>
									<td id="doctor"></td>
								</tr>
								<tr id="Attach_clinic">
									<td class="text-right" id="ClinicLabel">Прикреплен:</td>
									<td></td>
									<td id="ClinicID"></td>
								</tr>
							</table>
							<div id="shedown" style="display:none;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>Загрузка расписания...</div>
							<div id="sendform" style="display:none;">
								<strong><p id="SelectDate">Выберите дату и время приема:</p></strong>
									<select id="dates">
									</select>
									<select id="times">
									</select>
								<p id="acsDate">*Выводятся только доступные для записи дата и время</p>
								<div class="g-recaptcha" data-sitekey="6Letc0EUAAAAABVUj3056tjlxHh46-cuSPE1_z2E"></div>
								<button class="btn" id="app">Подтвердить и записаться</button>
								<br /><div class="back"><a href="index.php" ><-Назад</a></div>
							</div>
						</div>
						<div id="talon" style="display:none;" class="talon">
							<div class="sc_talon-body">
								<h3 id="LabelTalon">Талон №</h3> <span id="receipt"></span>
								<p id="TalonFio">Фамилия, имя, отчество</p>
								<strong><span id="t_fio"></span></strong>
								<p id="appear">Явится</p>
								<strong><span id="t_date"></span>  </strong>  <span id="TalonTime">Время</span>  <strong> <span id="t_time"></span></strong>
								<p id="TalonDoctorLabel">Врач</p>
								<strong><span id="t_doctor"></span></strong>
							</div>
						</div>
						<input type="hidden" id="language" name="language" value="1" /><!-- В value язык по умолчанию 1-рус 2-каз -->
						<div class="alert" id="t_error" style="display:none;">
							<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
							<span id="t_ernote">Текст ошибки</span>
						</div>
						
						<div class="success" id="success" style="display:none;">
							<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
							<span id="t_scnote">Текст успешного выполнения</span>
						</div> 	
						<div style="font-size: 24px; line-height: 1.5em;" style="display:none;" id="PrintBlock">
							<i class="fa fa-print" aria-hidden="true" id="iconPrint"></i><span class="sr-only">Example of print</span><a id="print" onclick="print()"> Распечатать талон</a>
						</div>
					</div>
					<div class="talon-footer"></div>
				</div>	
			</div>
		</div>
		
	</div>
	
	</div>
	<div class="footer hidden-xs">
		<div class="container text-center">
			<p id="footer">Медицинская информационная система "НАДЕЖДА", разработчик ИП "PROFit" </p>
		</div>
	</div>
	
	
	<script src="js/scripts.min.js"></script>
	<?php if(isset($_GET['vision'])) { ?>
	<link rel="stylesheet" href="css/poorvision.css">
	<?php } else { ?>
	<link rel="stylesheet" href="css/main.min.css">
	<?php } ?>
	

</body>
</html>
