$(function() {
	
	function supports_local_storage() {//функция проверяет поддерживает ли бразуер localStorage
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
			} catch(e){
			return false;
			}
		}
	var lStorage = supports_local_storage();//содержит true если браузер поддерживает localStorage
	
	$(document).ready(function(){
		
		//sessionStorage.clear();//каждая новая загрузка обнуляет storage
		 var onloadCallback = function() { //captcha render
        grecaptcha.render('g-recaptcha', {
          'sitekey' : 'your_site_key',
		  'hl' : 'ar',
        });
      };


	});	
if(lStorage == true){	
	////////////////////////Переключение языков////////////////////////////////////	
		$('#ru').click(function () {
			
			var lang = $('#ru').attr( "id");
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: "/ajax/lang.php",
				data: {lang:lang}
			}).done(function( result )
				{
					$('#ru').addClass("active");
					$('#kz').removeClass("active");
					$("#language").val('1');
					$('title').text('Запись на приём online');
					$('#langselect').html(result.langselect);
					$("#iin").attr( 'placeholder', result.IIN);
					$("#iin_label").html(result.iin_label);
					$("#clinic").html(result.clinic);
					$("#recordOnline").html(result.recordOnline);
					$("#confirm").html(result.confirm+' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>');
					$("#info").html(result.info);
					$("#FIOLabel").html(result.FIOLabel);
					$("#DoctorLabel").html(result.DoctorLabel);
					$("#LabelRecord").html(result.LabelRecord);
					$("#LabelTerritory").html(result.LabelTerritory);
					$("#SelectDate").html(result.SelectDate);
					$("#acsDate").html(result.acsDate);
					$("#app").html(result.app);
					$("#LabelTalon").html(result.LabelTalon);
					$("#TalonFio").html(result.TalonFio);
					$("#TalonTime").html(result.TalonTime);
					$("#TalonDoctorLabel").html(result.TalonDoctorLabel);
					$("#footer").html(result.footer);
					$("#print").html(result.print);
					$("#appear").html(result.appear);
					$(".back").html("<a href='index.php' >"+result.back+"</a>");
				});
		});
		$('#kz').click(function () {
			var lang = $('#kz').attr( "id");
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: "/ajax/lang.php",
				data: {lang:lang}
			}).done(function( result )
				{
					$('#ru').removeClass("active");
					$('#kz').addClass("active");
					$("#language").val('2');
					$('title').text('Online түрде қабылдауға жазылу');
					$('#langselect').html(result.langselect);
					$("#iin").attr( 'placeholder', result.IIN);
					$("#iin_label").html(result.iin_label);
					$("#clinic").html(result.clinic);
					$("#recordOnline").html(result.recordOnline);
					$("#confirm").html(result.confirm+' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>');
					$("#info").html(result.info);
					$("#FIOLabel").html(result.FIOLabel);
					$("#DoctorLabel").html(result.DoctorLabel);
					$("#LabelRecord").html(result.LabelRecord);
					$("#LabelTerritory").html(result.LabelTerritory);
					$("#SelectDate").html(result.SelectDate);
					$("#acsDate").html(result.acsDate);
					$("#app").html(result.app);
					$("#LabelTalon").html(result.LabelTalon);
					$("#TalonFio").html(result.TalonFio);
					$("#TalonTime").html(result.TalonTime);
					$("#TalonDoctorLabel").html(result.TalonDoctorLabel);
					$("#footer").html(result.footer);
					$("#print").html(result.print);
					$("#appear").html(result.appear);
					$(".back").html('<a href=index.php>'+result.back+'</a>');
				});
		});
/////////////////////// конец переключения языков //////////////////////
/////////////////////// обработка запроса ИИН///////////////////////////
	$('#confirm').click(function () {
			$('#Attach_clinic').hide();
			sessionStorage.clear(); //каждый новый запрос начнем с пустого storage
			$('.g-recaptcha div').css('width','auto');
			var iin = $('#iin').val();
			if (iin == ""){
				$('#ernote').remove();//очистим текст ошибки
				var globallang = $('#language').val();
				if(globallang == "1" ){
					$('#error').append('<span id="ernote">Ошибка! Введите ИИН!</span>');
				}
				else if (globallang == "2"){
					$('#error').append('<span id="ernote">Қате! ЖСН енгізіңіз</span>');
				}
				$('#error').fadeIn(700);
				$('#error').fadeOut(700);
			}
			else {
			
			$('#loading').show();
			$('#error').css('display','none');
			$('#mainform').css('display','none');
			$('#sendform').css('display','none');
			//sessionStorage.clear(); // очистим storage
			$.ajax({
				type: "POST",
				dataType: 'json',
				timeout:10000,
				url: "/ajax/GetPatient.php",
				data: {iin:iin}
			}).done(function( main )
				{
					$("#PrintBlock").css("display", "none");
					if(main.ErrorCode == ""){//ошибок нет
						 //если можно записатся
							$('#iinform').css('display','none');
							$('#fio').html(main.FIO);
							$('#territory').html(main.Territory);
							$('#doctor').html(main.Doctor);
							$('#mainform').fadeIn(700);
							
							//////////поместим переменные в сессионный storage
							sessionStorage.setItem('iin', iin);
							sessionStorage.setItem('DoctorID', main.DoctorID);
							sessionStorage.setItem('FIO', main.FIO);
							sessionStorage.setItem('Doctor', main.Doctor);
							sessionStorage.setItem('RegAvailable', main.RegAvailable);
							$('#loading').hide();
							$('#info').css('display','none');
							$('#shedown').fadeIn(700); //изображение загрузчика
							
							/////////////////////получаем расписание врача////////////////
								var DoctorID = sessionStorage.getItem('DoctorID');
								$('#dates').find('option').remove(); //удалим старые значения дат
								if(DoctorID!==""){
								$.ajax({
									type: "POST",
									dataType: 'json',
									url: "/ajax/GetShedule.php",
									data: {DoctorID:DoctorID}
								}).done(function( shedule )
									{
										var RegAvailable = sessionStorage.getItem('RegAvailable');
										$('#shedown').css('display', 'none');
										var index;
										for (index = 0; index < shedule.Dates.length; ++index) {//заполним даты приема
											
											//console.log(shedule.Dates[index].DateView);
											$("#dates").append( $('<option value='+index+'>'+shedule.Dates[index].DateView+'</option>'));
											
										}
										var tindex;
										for(tindex = 0; tindex <shedule.Dates[0].Times.length; ++tindex){//заполним время приема на сегодня
											$("#times").append( $('<option value='+shedule.Dates[0].Times[tindex].TimeEnd+'>'+shedule.Dates[0].Times[tindex].TimeStart+'</option>'));// в value добавим время окончания
											//console.log(shedule.Dates[0].Times[tindex]);
										}
										//занесем весь объект в sessionstorage
										sessionStorage["shedule.Dates"] = JSON.stringify(shedule.Dates);
										if(RegAvailable == "0"){
												$("#app").css("display", "none"); //пока оставим так, в боевом режиме убрать
												$('#t_ernote').remove();//очистим текст ошибки
												$('#t_error').append('<span id="t_ernote">Сервис временно не доступен</span>');
												$('#t_error').fadeIn(700);
											}	
										$('#sendform').fadeIn(700);
										$("#PrintBlock").css("display", "none");
										
											
									});
								}
								else {
									alert("error DoctorID!");
								}
							/////////////////////конец получения расписание врача////////////////
							
					
					}///////////////////Обработка ошибок/////////
					else if(main.ErrorCode == "100"){
						
						$('#ernote').remove();//очистим текст ошибки
						var globallang = $('#language').val();
							if(globallang == "1" ){
								$('#error').append('<span id="ernote">ошибка при формировании данных пациента, проверте введенный ИИН</span>');
							}
							else if (globallang == "2"){
								$('#error').append('<span id="ernote">науқастың деректерін қалыптастыру кезінде қате туындады, енгізілген ЖСН-ді тексеріңіз</span>');
								}

						$('#error').fadeIn(700);
						$('#loading').css('display','none');
					}
					else if(main.ErrorCode == "110"){
						$('#ernote').remove();//очистим текст ошибки
						var globallang = $('#language').val();
							if(globallang == "1" ){
								$('#error').append('<span id="ernote">ошибка проверки доступности записи в МО (разрешена ли запись пациента в МО прикрепления?)</span>');
							}
							else if (globallang == "2"){
								$('#error').append('<span id="ernote">МҰ-да жазылу мүмкіндігін тексеруде қате туындады (науқастың тіркелген МҰ-да жазылуына рұқсат етілген бе?</span>');
								}
						$('#error').fadeIn(700);
						$('#loading').css('display','none');
					}
					else if(main.ErrorCode == "120"){
						$('#ernote').remove();//очистим текст ошибки
						var globallang = $('#language').val();
							if(globallang == "1" ){
								$('#error').append('<span id="ernote">ошибка, не найдена МО прикрепления пациента</span>');
							}
							else if (globallang == "2"){
								$('#error').append('<span id="ernote">қате, науқастың тіркелген МҰ табылмады</span>');
								}
						$('#error').fadeIn(700);
						$('#loading').css('display','none');
					}
					else if(main.ErrorCode == "130"){
						$('#ernote').remove();//очистим текст ошибки
						var globallang = $('#language').val();
							if(globallang == "1" ){
								$('#error').append('<span id="ernote">ошибка, не найден участок прикрепления пациента</span>');
							}
							else if (globallang == "2"){
								$('#error').append('<span id="ernote">қате, науқастың тіркелген учаскесі табылмады</span>');
								}
						$('#error').fadeIn(700);
						$('#loading').css('display','none');
					}
					else if(main.ErrorCode == "140"){
						$('#ernote').remove();//очистим текст ошибки
						var globallang = $('#language').val();
							if(globallang == "1" ){
								$('#fio').html(main.FIO);
								$('#territory').html(main.Territory);
								$('#doctor').html(main.ErrorDesc);
								$('#Attach_clinic').show();
								$('#ClinicID').html(main.Attachment);
								$('#error').append('<span id="ernote">ошибка, не найден участковый врач пациента</span>');
								$('#mainform').fadeIn(700);
							}
							else if (globallang == "2"){
								$('#error').append('<span id="ernote">қате, науқастың учаскелік дәрігері табылмады</span>');
								}
						$('#error').fadeIn(700);
						$('#loading').css('display','none');
					}
					
				}).fail(function() { //если запрос не удался по каким либо причинам
					
					alert("Сервер не отвечает, попробуйте позже");
					location.reload();
					
				});
			}
		});
		$('#iin').keypress(function(e){ //отправка ИИН по клавише Enter
			if(e.which == 13){//код клавиши Enter
			 $('#confirm').click();//срабатывает функция клик кнопки
			}
		});
	
	/////////////////////// конец обработки запроса ИИН///////////////////////////
	
	////////////////////// обработка данных в выбираемых полях дата/время ///////
	$("#dates").change(function () {//при изменении даты нужно заново получить время приема
			var selectdata = $("#dates").val();
			var Storage = JSON.parse(sessionStorage["shedule.Dates"]);
			var pindex;
			$('#times').find('option').remove(); //удалим старые значения
				for(pindex = 0; pindex < Storage[selectdata].Times.length; ++pindex){//заполним новые
							$("#times").append( $('<option value='+Storage[selectdata].Times[pindex].TimeEnd+'>'+Storage[selectdata].Times[pindex].TimeStart+'</option>'));// В значение value запишем параметр "EndTime"
							//console.log(Storage[selectdata].Times[pindex]);
						}
	});
	///////////////////// конец обработки //////////////////////////////////////////
	
	//////////////////// обработка записи в базу и получения результата ////////////
	
	$("#app").click(function(){
		var captcha = grecaptcha.getResponse();
		//console.log(captcha);
		if (!captcha.length) {
			 // Выводим сообщение об ошибке
			var language = $("#language").val();
				if (language == "1"){
					alert('* Вы не прошли проверку "Я не робот"');
				}
				else if(language == "2"){
					alert('Сіз "Мен робот емеспін" тексерісін өтпедіңіз');
				}
			}
		else {	
					var language = $("#language").val();
					var iin = sessionStorage.getItem('iin');
					var DoctorID = sessionStorage.getItem('DoctorID');
					var StartTime = $("#times option:selected").text();
					var EndTime = $("#times").val();
					//теперь нужно получить текущие значения Date и CabinetID
					var Storage = JSON.parse(sessionStorage["shedule.Dates"]);
						var selectdata = $("#dates").val(); //значение индекса выбранной даты
					var Date = Storage[selectdata].Date;
					var	CabinetID = Storage[selectdata].CabinetID;
					var DateView = Storage[selectdata].DateView;
					$.ajax({
						type: "POST",
						dataType: 'json',
						timeout:8000,
						url: "/ajax/SaveAppointment.php",
						data: {iin:iin, DoctorID:DoctorID, StartTime:StartTime, EndTime:EndTime, Date:Date, CabinetID:CabinetID, language:language, captcha:captcha}
						}).done(function( MainResult )
							{
								if (MainResult.ErrorCode==""){//если запрос выполнился без ошибок
									
									var FIO = sessionStorage.getItem('FIO');
									var Doctor = sessionStorage.getItem('Doctor');
									//alert(MainResult.ReceiptNumber);
									$('.talon-body').css('display', 'none');
									$('#talon').fadeIn(700);
									$('#receipt').html(MainResult.ReceiptNumber);
									$('#t_fio').html(FIO);
									$('#t_date').html(DateView);
									$('#t_time').html(StartTime);
									$('#t_doctor').html(Doctor);
										$('#t_error').css("display", "none");
										$('#t_scnote').remove();//очистим текст 
										if(language =="1"){
											$('#success').append('<span id="t_scnote">Запись прошла успешно</span>');
										}
										else if(language =="2"){
											$('#success').append('<span id="t_scnote">Жазылу сәтті аяқталды</span>');
										}
										$('#success').fadeIn(700);
										$('#PrintBlock').css('display', 'block');
										//$('#success').fadeOut(700);
								}
								else {
									
									$('#t_ernote').remove();//очистим текст ошибки
									$('#t_error').append('<span id="t_ernote">'+MainResult.ErrorDesc+'</span>');
									$('#t_error').fadeIn(700);
									grecaptcha.reset(); //обнулим каптча после ошибки
									//$('#t_error').fadeOut(700);
									console.log(MainResult.ErrorCode);
								}
							}).fail(function()
							{
								alert("Сервер не отвечает, попробуйте позже");
								location.reload();
							});
		}					
	});
	
	///////////////////  конец обработки записи в базу /////////////////////////////
	
}
else {
	alert("LocalStorage (HTML5) not support, please update your browser");
}	
	
});// end of all scripts
