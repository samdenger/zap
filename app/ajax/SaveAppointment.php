<?php
// ваш секретный ключ
$secret = '6Letc0EUAAAAALsAPQeEtIgzA19yWCYm-cV6pT0q';
// однократное включение файла autoload.php (клиентская библиотека reCAPTCHA PHP)
require_once (dirname(__FILE__).'/recaptcha/autoload.php');
// если в массиве $_POST существует ключ g-recaptcha-response, то...
if (isset($_POST['captcha'])) {
	
  $recaptcha = new \ReCaptcha\ReCaptcha($secret);
  // получить результат проверки кода recaptcha
  $resp = $recaptcha->verify($_POST['captcha'], $_SERVER['REMOTE_ADDR']);
	
	if ($resp->isSuccess()){
		if(isset($_POST["iin"])){
		
			require_once("../config.php");
			require_once("../classes/appointment.php");
			$iin = trim($_POST["iin"]);// если вдруг пришло с пробелами
			$DoctorID = $_POST["DoctorID"];
			$CabinetID = $_POST["CabinetID"];
			if(empty($CabinetID)){
				$CabinetID = "0";
			}
			$StartTime = $_POST["StartTime"];
			$EndTime = $_POST["EndTime"];
			$Date = $_POST["Date"];
			$language = $_POST["language"];

			$records = new appoint($config);
			$json = $records->SaveAppointment1($iin,$DoctorID,$CabinetID,$StartTime,$EndTime,$Date,$language);
			$error = $json->ErrorCode;
			//$data = json_decode($json);
			//echo json_encode($json);
				if ($error=="0"){
					$talon = $json->ReceiptNumber;
					$data = array("ErrorCode" => $error, "ReceiptNumber" => $talon );
					echo json_encode($data);
				}
				else {
				
					$error_desc = $json->ErrorDesc;
					$data = array("ErrorCode" => $error,"ErrorDesc" => $error_desc);
					echo json_encode($data);
					
				}
		}
	}
	else {
		$error = $resp->getErrorCodes();
		$error_desc = "Код captcha не прошел проверку на сервере";
		$data = array("ErrorCode" => $error,"ErrorDesc" => $error_desc);
		echo json_encode($data);
	}
}
?>

