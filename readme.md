*Онлайн запись на прием*

Как работать с репозиторием: 

*Устанавливаем node.js
*Устанавливаем git
*Устанавливаем gulp

Заходим в папку с проектами
В консоле:

*git clone https://samdenger@bitbucket.org/samdenger/zap.git
*cd zap
*npm i (инициализация)
*gulp (начало работы)

Комиты:
*git add .
*git commit -m "коммент"

Выгрузить в репозиторий
*git push
Получить из репозитория
*git pull

